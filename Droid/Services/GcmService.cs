﻿using System;
using Android.App;
using Android.Content;
using Gcm.Client;
using WindowsAzure.Messaging;
using Java.Lang;


namespace DemoPushNotification.Droid.Services
{
	[BroadcastReceiver (Permission = Constants.PERMISSION_GCM_INTENTS)]
	[IntentFilter (new [] { Constants.INTENT_FROM_GCM_MESSAGE }, Categories = new [] { "@PACKAGE_NAME@" })]
	[IntentFilter (new [] { Constants.INTENT_FROM_GCM_REGISTRATION_CALLBACK }, Categories = new [] { "@PACKAGE_NAME@" })]
	[IntentFilter (new [] { Constants.INTENT_FROM_GCM_LIBRARY_RETRY }, Categories = new [] { "@PACKAGE_NAME@" })]
	public class NotificationsBroadCastReceiver : GcmBroadcastReceiverBase<PushNotificaionHandlerService>
	{
		public static string [] SenderIDs = { "484591629793" };
	}



	[Service]
	public class PushNotificaionHandlerService : GcmServiceBase { 
	
		private string _conectionString =
			ConnectionString.
			                CreateUsingSharedAccessKeyWithFullAccess  (
								new Java.Net.URI ("sb://xddpr2016ns.servicebus.windows.net/"),"Flry+iECxx+L18ifQgCLphKtIz3UjeooxDuidSP7+x0=");

		public string _hubName = "xddpr2016hub";



		public static string RegistrationID { get; private set; }



		public PushNotificaionHandlerService () :base(NotificationsBroadCastReceiver.SenderIDs)
		{

		}

		protected override void OnError (Context context, string errorId)
		{
			Console.WriteLine ("OnError => " + errorId);
		}

		protected override void OnMessage (Context context, Intent intent)
		{
			var title = "Universal App";

			if (intent.Extras.ContainsKey ("title")) {
				title = intent.Extras.GetString ("title");
			}

			var messageText = intent.Extras.GetString ("message");

			if (!string.IsNullOrEmpty (messageText)) {
				CreateNotification (title, messageText);
			}
		}

		void CreateNotification (string title, string messageText)
		{
			const int pendingItentId = 0;
			const int notificationId = 0;


			var startupIntent = new Intent (this, typeof (MainActivity));
			var stackBuilder = TaskStackBuilder.Create (this);

			stackBuilder.AddParentStack (Class.FromType (typeof (MainActivity)));
			stackBuilder.AddNextIntent (startupIntent);

			var pendingIntent = stackBuilder.GetPendingIntent (pendingItentId, PendingIntentFlags.OneShot);

			var builder = new Notification.Builder (this)
										  .SetContentIntent (pendingIntent)
										  .SetContentTitle (title)
										  .SetContentText (messageText)
			                              .SetSound (Android.Media.RingtoneManager.GetDefaultUri (Android.Media.RingtoneType.Notification))
			                              .SetVibrate(new long [] {1000, 1000, 1000, 1000, 1000})
			                              .SetSmallIcon (Resource.Drawable.icon);


			var notification = builder.Build ();
			notification.Flags = NotificationFlags.AutoCancel;

			var notificationManager = GetSystemService (NotificationService) as NotificationManager;

			notificationManager.Notify (notificationId, notification);

		}

		protected override void OnRegistered (Context context, string registrationId)
		{
			var hub = new NotificationHub (_hubName, _conectionString, context);

			var tags = new string [] { "DevDays", "JGreg" };

			hub.Register (registrationId, tags);
		}

		protected override void OnUnRegistered (Context context, string registrationId)
		{
			var hub = new NotificationHub (_hubName, _conectionString, context);

			hub.UnregisterAll (registrationId);
		}
	}
}
