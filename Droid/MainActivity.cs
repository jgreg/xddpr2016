﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Gcm.Client;
using DemoPushNotification.Droid.Services;

namespace DemoPushNotification.Droid
{
	[Activity (Label = "DemoPushNotification.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);


			try {
				System.Diagnostics.Debug.WriteLine ("Registering");

				GcmClient.CheckDevice (this);
				GcmClient.CheckManifest (this);
				GcmClient.Register (this, NotificationsBroadCastReceiver.SenderIDs);

			} catch (Java.Net.MalformedURLException ex) {
				System.Diagnostics.Debug.WriteLine ("Wrong url" + ex.Message);

			} catch (Exception ex) {
				System.Diagnostics.Debug.WriteLine ("Ex =>" + ex.Message);
			}

			LoadApplication (new App ());
		}
	}
}
